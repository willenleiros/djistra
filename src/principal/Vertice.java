package principal;

public class Vertice {
	
	private Vertice proxVertice;
	private Vertice antVertice;
	private Aresta proxAresta;
	private Object chave;
	private int grau=0;
	private boolean passou=false;
	private boolean encontrado=false;
	
	public Vertice(Object object){
		super();
		this.chave=object;
	}	
	public Vertice getProxVertice() {
		return proxVertice;
	}
	public void setProxVertice(Vertice proxVertice) {
		this.proxVertice = proxVertice;
	}
	public Vertice getAntVertice() {
		return antVertice;
	}
	public void setAntVertice(Vertice antVertice) {
		this.antVertice = antVertice;
	}
	public Aresta getProxAresta() {
		return proxAresta;
	}
	public void setProxAresta(Aresta proxAresta) {
		this.proxAresta = proxAresta;
	}
	public Object getChave() {
		return chave;
	}
	public void setChave(Object chave) {
		this.chave = chave;
	}
	public int getGrau() {
		return grau;
	}
	public void setGrau(int grau) {
		this.grau = grau;
	}
	public boolean isPassou() {
		return passou;
	}
	public void setPassou(boolean passou) {
		this.passou = passou;
	}
	public boolean isEncontrado() {
		return encontrado;
	}
	public void setEncontrado(boolean encontrado) {
		this.encontrado = encontrado;
	}
	@Override
	public String toString() {
		return this.chave.toString();
	}

}
