package principal;

public class Aresta {
	
	private Vertice vertice_1;
	private Vertice vertice_2;
	private double custo;
	private Aresta proxAresta;
	private Aresta antAresta;
	private boolean direcionado;
	
	public Aresta(Vertice v1, Vertice v2, boolean d, double custo){
		super();
		this.custo=custo;
		this.vertice_1=v1;
		this.vertice_2=v2;
		this.direcionado=d;
	}
	public Vertice getVertice_1() {
		return vertice_1;
	}
	public void setVertice_1(Vertice vertice_1) {
		this.vertice_1 = vertice_1;
	}
	public Vertice getVertice_2() {
		return vertice_2;
	}
	public void setVertice_2(Vertice vertice_2) {
		this.vertice_2 = vertice_2;
	}
	public double getCusto() {
		return custo;
	}
	public void setCusto(double custo) {
		this.custo = custo;
	}
	public Aresta getProxAresta() {
		return proxAresta;
	}
	public void setProxAresta(Aresta proxAresta) {
		this.proxAresta = proxAresta;
	}
	public Aresta getAntAresta() {
		return antAresta;
	}
	public void setAntAresta(Aresta antAresta) {
		this.antAresta = antAresta;
	}
	public boolean getIsDirecionado() {
		return direcionado;
	}
	public void setIsDirecionado(boolean direcionado) {
		this.direcionado = direcionado;
	}
	@Override
	public String toString() {
		return String.valueOf(this.custo);
	}	

}
