package principal;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Grafo grafo = new Grafo();
		
		grafo.inserirVertice("Globo");
		grafo.inserirVertice("Record");
		grafo.inserirVertice("SBT");
		grafo.inserirVertice("Band");
		grafo.inserirAresta("Globo", "Record", false, 40);
		grafo.inserirAresta("Record", "SBT", false, 60);
		grafo.inserirAresta("SBT", "Band", false, 110);
		grafo.inserirAresta("Band", "Globo", false, 90);
		grafo.inserirAresta("Globo", "SBT", false, 150);
		
		Dijkstra dijkstra = new Dijkstra();
		double distancia = dijkstra.Djistra(grafo, "Globo");
		System.out.println("Distancia: "+String.valueOf(distancia));
		
//		System.out.println("------Vertices------");
//		//grafo.imprimirGrafo();
//		System.out.println("------Arestas------");
//		//grafo.arestas();
//		System.out.println("------Matriz de Adjacencia------");
//		grafo.imprimirMatrizAdjacencia(grafo.arestas(), grafo.vertices());
		
		

	}

}
