package principal;

public class Grafo {
	
	private int qtdVertice=0;
	public Vertice inicio=null;
	private Vertice finalGrafo=null;

	private Aresta[] arestasIguais= new Aresta[2];
	private Vertice[] vertices=new Vertice[2];
	private Aresta[] arestasInicidentes= new Aresta[10];
	private Vertice[] todosVertices=new Vertice[this.qtdVertice];
	private Aresta[] todasArestas=new Aresta[100];


	//-----------------------------------------------------------------------------------
	//Setor Vertice

	public void inserirVertice(Object object){
		Vertice v = new Vertice(object);
		if(this.inicio==null){
			this.inicio=v;
			this.finalGrafo=v;
			v.setAntVertice(null);
			v.setProxVertice(null);
		}else{
			this.finalGrafo.setProxVertice(v);
			v.setAntVertice(this.finalGrafo);
			this.finalGrafo=v;
		}
		this.qtdVertice++;	
		Vertice[] todosVertices=new Vertice[this.qtdVertice];
		this.todosVertices=todosVertices;
	}

	public void retirarVertice(Object chave){
		Vertice aRetirar=procurarVertice(chave, this.inicio);
		destruirArestasVertice(aRetirar, this.inicio);
		excluirVertice(aRetirar);	
	}

	private void excluirVertice(Vertice v){
		if(v==this.inicio){
			this.inicio=v.getProxVertice();
			v.getProxVertice().setAntVertice(null);
			v.setProxVertice(null);
		}else if(v==finalGrafo){
			this.finalGrafo=v.getAntVertice();
			v.getAntVertice().setProxVertice(null);
			v.setAntVertice(null);
		}else{
			v.getAntVertice().setProxVertice(v.getProxVertice());
			v.getProxVertice().setAntVertice(v.getAntVertice());
		}
		v=null;
		qtdVertice--;
		Vertice[] todosVertices=new Vertice[this.qtdVertice];
		this.todosVertices=todosVertices;
	}

	public Vertice procurarVertice(Object i, Vertice v){
		if(v.getChave().equals(i))
			return v;
		else
			return procurarVertice(i, v.getProxVertice());
	}

	private void destruirArestasVertice(Vertice aRetirar, Vertice v){
		Aresta aux;
		while(v!=null){
			if(v.getProxAresta()!=null){
				aux=v.getProxAresta();
				while(aux!=null){
					aux=destruirArestas(aux,aRetirar);
				}
			}
			v=v.getProxVertice();
		}
	}

	private Aresta destruirArestas(Aresta a, Vertice aRetirar){
		Aresta aux=null;
		if(a.getVertice_1()==aRetirar || a.getVertice_2()==aRetirar){
			if(a.getAntAresta()!=null||a.getProxAresta()!=null){
				if(a.getAntAresta()!=null&&a.getProxAresta()!=null){
					aux=a.getProxAresta();
					a.getAntAresta().setProxAresta(a.getProxAresta());
					a.getProxAresta().setAntAresta(a.getAntAresta());
				}else if(a.getAntAresta()!=null){
					aux=null;
					a.getAntAresta().setProxAresta(null);
					a.setAntAresta(null);
				}else if(a.getProxAresta()!=null){
					aux=a.getProxAresta();
					a.getProxAresta().setAntAresta(null);
					a.getVertice_1().setProxAresta(a.getProxAresta());
					a.setProxAresta(null);
				}
			}else{
				aux=null;
				a.getVertice_1().setProxAresta(null);
			}
			a.getVertice_1().setGrau(a.getVertice_1().getGrau()-1);
			a=null;
		}else{
			if(a.getProxAresta()!=null){
				aux=a.getProxAresta();
			}else{
				aux=null;
			}
		}
		return aux;
	}

	// Final Setor Vertice
	//-----------------------------------------------------------------------------------
	// Inicio Setor Aresta

	public void inserirAresta(Object idV1, Object idV2, boolean direc, double custo){
		Vertice v1=procurarVertice(idV1, this.inicio);
		Vertice v2=procurarVertice(idV2, this.inicio);
		Aresta a1=null,a2=null;
		Aresta a=new Aresta(v1,v2,direc,custo);
		Aresta a_cop=new Aresta(v2,v1,direc,custo);
		if(v1.getProxAresta()!=null)
			a1=ultimaArestaVertice(v1.getProxAresta());
		if(v2.getProxAresta()!=null)
			a2=ultimaArestaVertice(v2.getProxAresta());
		if(direc==true){
			if(a1!=null){
				a1.setProxAresta(a);
				a.setAntAresta(a1);
			}else{
				v1.setProxAresta(a);
			}
			v1.setGrau(v1.getGrau()+1);
		}else{
			if(a1!=null&&a2!=null){
				a1.setProxAresta(a);
				a.setAntAresta(a1);
				a2.setProxAresta(a_cop);
				a_cop.setAntAresta(a2);
			}else if(a1!=null){
				a1.setProxAresta(a);
				a.setAntAresta(a1);
				v2.setProxAresta(a_cop);
			}else if(a2!=null){
				v1.setProxAresta(a);
				a2.setProxAresta(a_cop);
				a_cop.setAntAresta(a2);
			}else{
				v1.setProxAresta(a);
				v2.setProxAresta(a_cop);
			}
			v1.setGrau(v1.getGrau()+1);
			v2.setGrau(v2.getGrau()+1);
		}
	}

	public Aresta ultimaArestaVertice(Aresta a){
		if(a.getProxAresta()!=null){
			a=a.getProxAresta();
			return ultimaArestaVertice(a);
		}
		return a;
	}

	private boolean buscarAresta(Vertice v, double c, Aresta a){
		boolean isReal=false;
		if(a!=null){
			if(a.getVertice_2()==v&&a.getCusto()==c){
				if(arestasIguais[0]==null){
					arestasIguais[0]=a;
					isReal=true;
					return isReal;
				}else{
					arestasIguais[1]=a;
					isReal=true;
					return isReal;
				}
			}
			return buscarAresta(v, c, a.getProxAresta());
		}
		return isReal;
	}

	private boolean buscarArestaVertice(Vertice v, Aresta a){
		if(a!=null){
			if(a.getVertice_2()==v){
				return true;					
			}
		}
		return buscarArestaVertice(v, a.getProxAresta());
	}

	public Aresta procurarAresta(Vertice v, Vertice v1, Vertice v2, double custo){
		Aresta aux=v.getProxAresta();
		while(aux!=null){
			if(aux.getVertice_1()==v1&&aux.getVertice_2()==v2&&aux.getCusto()==custo){
				System.out.println(aux.getCusto());
				return aux;
			}
			aux=aux.getProxAresta();
		}
		return procurarAresta(v.getProxVertice(), v1, v2, custo);
	}

	public void removerAresta(Object v1, Object v2, double custo){
		Vertice v_1=procurarVertice(v1, this.inicio);
		Vertice v_2=procurarVertice(v2, this.inicio);
		if(v_1.getProxAresta()!=null)
			buscarAresta(v_2, custo, v_1.getProxAresta());
		if(v_2.getProxAresta()!=null)
			buscarAresta(v_1, custo, v_2.getProxAresta());
		Aresta a1=null,a2=null;
		if(arestasIguais[0]!=null){
			a1=arestasIguais[0];
		}
		if(arestasIguais[1]!=null){
			a2=arestasIguais[1];
		}
		if(a1!=null&&a2!=null){
			if(a1.getAntAresta()!=null&&a1.getProxAresta()!=null){
				a1.getProxAresta().setAntAresta(a1.getAntAresta());
				a1.getAntAresta().setProxAresta(a1.getProxAresta());
			}else if(a1.getAntAresta()!=null){
				a1.getAntAresta().setProxAresta(null);
			}else if(a1.getProxAresta()!=null){
				a1.getProxAresta().setAntAresta(null);
				a1.getVertice_1().setProxAresta(a1.getProxAresta());
				a1.setProxAresta(null);
			}
			if(a2.getAntAresta()!=null&&a2.getProxAresta()!=null){
				a2.getProxAresta().setAntAresta(a2.getAntAresta());
				a2.getAntAresta().setProxAresta(a2.getProxAresta());
			}else if(a2.getAntAresta()!=null){
				a2.getAntAresta().setProxAresta(null);
			}else if(a2.getProxAresta()!=null){
				a2.getProxAresta().setAntAresta(null);
				a2.getVertice_1().setProxAresta(a2.getProxAresta());
				a2.setProxAresta(null);
			}
			v_1.setGrau(v_1.getGrau()-1);
			v_2.setGrau(v_2.getGrau()-1);
		}else if(a1!=null){
			if(a1.getAntAresta()!=null&&a1.getProxAresta()!=null){
				a1.getProxAresta().setAntAresta(a1.getAntAresta());
				a1.getAntAresta().setProxAresta(a1.getProxAresta());
			}else if(a1.getAntAresta()!=null){
				a1.getAntAresta().setProxAresta(null);
			}else if(a1.getProxAresta()!=null){
				a1.getProxAresta().setAntAresta(null);
				a1.getVertice_1().setProxAresta(a1.getAntAresta());
				a1.setProxAresta(null);
			}
			v_1.setGrau(v_1.getGrau()-1);
		}
		a1=a2=null;
		Aresta[] novoArray=new Aresta[2];
		arestasIguais=novoArray;
		return;
	}
	//------------------------------Resto do Tad Grafo-------------------------//

	public Vertice[] finalVertices(Aresta a) {
		if(a.getIsDirecionado()==false){
			vertices[0]=a.getVertice_1();
			vertices[1]=a.getVertice_2();
		}else{
			vertices[0]=a.getVertice_2();
			vertices[1]=null;
		}
		return vertices;
	}

	public Vertice oposto(Vertice v, Aresta a){
		if(a.getVertice_1()==v)
			return a.getVertice_2();
		else
			return a.getVertice_1(); 
	}

	public boolean ÈAdjacente(Vertice v1, Vertice v2) throws Exception{
		boolean aux;
		try{
			if(v1.getProxAresta()!=null)
				aux=buscarArestaVertice(v2, v1.getProxAresta());
			else if(v2.getProxAresta()!=null)
				aux=buscarArestaVertice(v1, v2.getProxAresta());
			else
				aux=false;

			return aux;
		}catch (java.lang.NullPointerException e){
			return false;
		}
	}

	public void substituirVertice(Vertice v, Object i) {
		v.setChave(i);
	}

	public void substituirAresta(Aresta a, double c) {
		if(a.getIsDirecionado()==true)
			a.setCusto(c);
		else{
			procurarAresta(this.inicio, a.getVertice_2(), a.getVertice_1(), a.getCusto()).setCusto(c);;
			a.setCusto(c);
		}
	}

	public Aresta[] arestasIncidentes(Vertice v) {
		int cont=0;
		Aresta aux=v.getProxAresta();
		if(aux!=null){
			while(aux!=null){
				arestasInicidentes[cont]=aux;
				cont++;
				System.out.println(aux.getCusto());
				aux=aux.getProxAresta();
			}
		}
		cont=0;
		return arestasInicidentes;
	}

	public Vertice[] vertices() {
		Vertice aux=this.inicio;
		for(int i=0;i<this.qtdVertice;i++){
			todosVertices[i]=aux;
			//System.out.println(aux.getChave().toString());
			aux=aux.getProxVertice();
		}
		return todosVertices;
	}
	public Aresta[] arestas() {
		vertices();
		int cont=0;
		Aresta aux;
		for(int i=0; i<this.qtdVertice;i++){
			aux=todosVertices[i].getProxAresta();
			if(aux!=null){
				while(aux!=null){
					todasArestas[cont]=aux;
					cont++;
					//System.out.println("Vertice1: "+aux.getVertice_1());
					//System.out.println("Vertice2: "+aux.getVertice_2());
					//System.out.println("Custo: "+aux.getCusto());
					aux=aux.getProxAresta();
				}
			}
		}
		return todasArestas;
	}
	public boolean ÈDirecionado(Aresta a){
		return a.getIsDirecionado();
	}

	public int ordem() {
		return qtdVertice;
	}

	public int grau(Vertice v){
		return v.getGrau();
	}

	public void imprimirGrafo(){
		Vertice aux=this.inicio;
		Aresta a_aux=aux.getProxAresta();
		while(aux!=null){
//			System.out.println("Vertice: Chave: "+aux.getChave().toString());
			System.out.println("Chave: "+aux.getChave().toString());
			System.out.println("Grau: "+aux.getGrau());
			System.out.println("Proxima aresta: "+aux.getProxAresta());
			System.out.println("Proximo Vertice: "+aux.getProxVertice());
			System.out.println("Anterior Vertice: "+aux.getAntVertice());
			while(a_aux!=null){
				//System.out.println("Aresta: Vertice: "+a_aux.getVertice_2().getChave()+" Custo: "+a_aux.getCusto());
				a_aux=a_aux.getProxAresta();
			}
			aux=aux.getProxVertice();
			if(aux!=null)
				a_aux=aux.getProxAresta();
		}
		System.out.println("----------------------------------------------------------");
	}

	//------------------------------Algoritmos Add Tad Grafo-------------------------//

	private Grafo popularNovoGrafo() {
		Vertice v=this.inicio;
		Grafo grafo=new Grafo();
		while(v!=null){
			grafo.inserirVertice(v.getChave());
			v=v.getProxVertice();
		}
		v=this.inicio;
		Aresta a;
		while(v!=null){
			if(v.getProxAresta()!=null){
				a=v.getProxAresta();
				while(a!=null){
					grafo.inserirAresta(a.getVertice_1().getChave(), a.getVertice_2().getChave(), a.getIsDirecionado(), a.getCusto());
					a=a.getProxAresta();
				}
			}
			v=v.getProxVertice();
		}
		return grafo;
	}
	
	private static void verificarExistenciaCaminhoEuleriano(Object[][] matriz){
		
//		int grau = 0;
//		int soma = 0;
//		int n = 0; //número de linha da matriz
//		int f = 0; //linha atual
//		Object matadj[][] = matriz;
//		
//		
//		
//		while(soma <= 2){
//			
//			grau = 0;
//			
//			for(Object obj : matriz){
//				
//			}
//			
//			for(int g=0; g < n; g++){
//				grau += matadj[f][g];
//			}
//			
//			if(grau % 2 == 1)
//				soma++;
//			
//			f++;
//		}
//		
//		while(soma <= 2 && f <= n){
//			
//			grau = 0;
//			
//			for(int g=0; g < n; g++){
//				grau += matadj[f][g];
//			}
//			
//			if(grau % 2 == 1)
//				soma++;
//			
//			f++;
//		}
//		
//		if(soma > 2)
//			System.out.println("\t---->Não existe caminho");
//		else
//			System.out.println("\t---->Existe caminho");
		
	}
	
	public void imprimirMatrizAdjacencia(Aresta[] arestas, Vertice[] vertices){
		
		int cont = 0;
		
		System.out.println("Total de vertices: "+vertices.length);
		
		for(int i=0; i < vertices.length; i++){
			System.out.println("Vertice: "+vertices[i].getChave());
		}
		
		for(int i=0; i < arestas.length; i++){ 
			if(arestas[i] != null){ 
				cont++; 
				System.out.println("Vertice1: "+arestas[i].getVertice_1());
				System.out.println("Vertice2: "+arestas[i].getVertice_2());
				System.out.println("Aresta: "+arestas[i].getCusto());
			}
		}
		
		System.out.println("Total de arestas: "+cont);
		
		int[][] matriz = new int[vertices.length][vertices.length];
		
		for(int i=0; i < cont - 1; i++){ 
			
			for(int j=0; j < cont - 2; j++){
				
				if(arestas[i] != null){
					
					if(arestas[i].getVertice_1() == arestas[j].getVertice_2()){
						System.out.println("1");
					}
					else{
						System.out.println("0");
					}
					
				}
				
			}
			
		}
		
	}

}
