package principal;

public class Dijkstra {
	
	public double Djistra(Grafo g,Object object){
		int somaDistancias=0;
		Vertice v=g.procurarVertice(object, g.inicio);
		Distancia inicioDistancia=null;
		Distancia auxDistancia=null;
		double aux=0;
		Aresta auxAresta=v.getProxAresta();
		boolean encontrou=false;
		int cont=1;
		Distancia menor=null;
		if(inicioDistancia==null){
			Distancia d=new Distancia();
			d.setCaminho(v);
			d.setDistancia(0);
			d.setLabel(v);
			inicioDistancia=d;
			auxDistancia=d;
		}
		v.setPassou(true);
		while(auxAresta!=null&&cont<g.ordem()){
			while(auxDistancia!=null){
				if(auxAresta.getVertice_2()==auxDistancia.getLabel()){
					encontrou=true;				
					if((auxAresta.getCusto()+aux)<auxDistancia.getDistancia()){
						auxDistancia.setCaminho(auxAresta.getVertice_1());
						auxDistancia.setDistancia(auxAresta.getCusto()+aux);
					}
				}
				auxDistancia=auxDistancia.getProx_vertice();
			}
			if(encontrou==false && auxAresta.getVertice_2().isPassou()==false && auxAresta.getVertice_2().isEncontrado()==false){
				Distancia d=new Distancia();
				auxAresta.getVertice_2().setEncontrado(true);
				d.setCaminho(auxAresta.getVertice_1());
				d.setDistancia(auxAresta.getCusto()+aux);
				d.setLabel(auxAresta.getVertice_2());
				auxDistancia=inicioDistancia;
				while(auxDistancia.getProx_vertice()!=null){
					auxDistancia=auxDistancia.getProx_vertice();
				}
				auxDistancia.setProx_vertice(d);
				auxDistancia=inicioDistancia;
			}
			auxDistancia=inicioDistancia;
			encontrou=false;
			if(auxAresta.getProxAresta()==null){
				int contador=0;
				while(auxDistancia!=null){
					if(auxDistancia.getLabel().isPassou()==false && auxDistancia.getDistancia()!=0){
						menor=auxDistancia;
						contador++;
					}
					if(auxDistancia.getLabel().isPassou()==false && auxDistancia.getDistancia()!=0 && auxDistancia.getDistancia()<menor.getDistancia() && contador==1){
						menor=auxDistancia;
					}
					auxDistancia=auxDistancia.getProx_vertice();
				}
				auxAresta=menor.getLabel().getProxAresta();
				menor.getLabel().setPassou(true);
				aux=menor.getDistancia();
				cont++;
			}else{
				auxAresta=auxAresta.getProxAresta();
			}

		}
		auxDistancia=inicioDistancia;
		while(auxDistancia!=null){
			somaDistancias+=auxDistancia.getDistancia();
			System.out.println(auxDistancia.getLabel().getChave().toString()+":"+auxDistancia.getCaminho().getChave().toString()+":"+auxDistancia.getDistancia());
			auxDistancia.getLabel().setPassou(false);
			auxDistancia.getLabel().setEncontrado(false);
			auxDistancia=auxDistancia.getProx_vertice();
		}			
		return somaDistancias;
	}

}
