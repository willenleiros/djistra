package principal;

public class Distancia {
	
	private Vertice label;
	private double distancia;
	private Vertice caminho;
	private Distancia prox_vertice;
	
	public Vertice getLabel() {
		return label;
	}
	public void setLabel(Vertice label) {
		this.label = label;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	public Vertice getCaminho() {
		return caminho;
	}
	public void setCaminho(Vertice caminho) {
		this.caminho = caminho;
	}
	public Distancia getProx_vertice() {
		return prox_vertice;
	}
	public void setProx_vertice(Distancia prox_vertice) {
		this.prox_vertice = prox_vertice;
	}

}
